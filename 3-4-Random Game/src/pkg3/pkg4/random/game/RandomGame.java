
package pkg3.pkg4.random.game;
import java.util.Random;
import java.util.Scanner;
public class RandomGame {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        final int MAX_NUM = 100;
                
        Random random = new Random ();

        int number1 = random.nextInt(MAX_NUM+1);
        int number2 = random.nextInt (MAX_NUM+1);
        
        System.out.printf("How much is %d+%d ?\n", number1, number2);
        int sum = input.nextInt();
        
        if (sum == number1 + number2){
            System.out.println("You are right.");
        } else {
            System.out.println("It is not correct! The correct answer is: " + (number1 + number2));
        }
    }
    
}
