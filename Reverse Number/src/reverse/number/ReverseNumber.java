package reverse.number;
import static java.lang.Integer.reverse;
import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Arrays;
public class ReverseNumber {

    public static int reverse(int number){
        int result = 0;
        while (number >0){
            int lastDigit = number % 10;
            result += lastDigit;
            number /= 10;
        }
    }
    
    
    public static void main(String[] args) {
int value = 1234;
int reverse = reverse(value);
    }
    
}
