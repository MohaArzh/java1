

package yearminute;

import java.util.Scanner;

public class YearMinute {

    public static void main(String[] args) {
        
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the number of minutes:");
        int minute = input.nextInt();
//        double year = minute / 525600;
//        System.out.println(minute + "is approximately " + year + "years.");
          double totalDay = minute / 1440;
          double year = minute / 525600;
          double dayRest = totalDay - year * 365;
          System.out.println(minute + " is approximately " + year + " year(s) and " + dayRest + " day(s).");
    }
    
}
