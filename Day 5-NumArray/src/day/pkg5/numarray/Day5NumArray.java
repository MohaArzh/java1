package day.pkg5.numarray;

import java.util.Scanner;
import java.util.Arrays;

public class Day5NumArray {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int[] valuesArray = new int[4];

        for (int i = 0; i < 4; i++) {
            System.out.println("Enter on number: ");
            int number = input.nextInt();
            valuesArray[i] = number;

        }

        System.out.println("Your numbers are: " + Arrays.toString(valuesArray));

        int gratest = valuesArray[0];

        for (int i = 0; i < valuesArray.length; i++) {
            int elementArray = valuesArray[i];
            if (elementArray > gratest) {
                gratest = elementArray;
            }
        }

        int smallest = valuesArray[0];

        for (int i = 0; i < valuesArray.length; i++) {
            int elementArray = valuesArray[i];
            if (elementArray < smallest) {
                smallest = elementArray;
            }
        }
        int sum = 0;
        for (int i = 0; i < valuesArray.length; i++) {
            int elementArray = valuesArray[i];
            sum += elementArray;
        }
        int arrayLengthNum = valuesArray.length;
        System.out.println("Yor gratest number is: " + gratest);
        System.out.println("Yor smallest number is: " + smallest);
        System.out.println("The sum of your numbers is: " + sum);
        System.out.printf("The average of your numbers is: %.2f \n", (double) sum / arrayLengthNum);

    }

}
