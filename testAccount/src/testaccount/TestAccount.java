
package testaccount;
import java.util.Scanner;
public class TestAccount {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("How much do you want to save each month?");
        double saving = input.nextDouble ();
        
        System.out.println("How many month do you want to save?");
        int month = input.nextInt ();
                
        final double MONTHLY_INTEREST = 0.00417;
        double money = saving;
        
        for (int i = 0; i < month; i++){
            money = money * 1.0 + MONTHLY_INTEREST;
            System.out.printf("Amount of money at month %d is %.2f\n", i+1, money);
            money += saving;
     
        }
        System.out.printf("Amount of your account at month %d will be: %.2f\n", month, money);

    }
    
}
