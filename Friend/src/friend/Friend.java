
package friend;
import java.util.Scanner;

public class Friend {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String name;
        int count = 0;
        do {
            System.out.print("Enter friend's name. Leave empty to finish.");
            name = input.nextLine();
            System.out.printf("%s is your friend\n", name);
            count++;
        }
        while (!name.eqauls(""));
        String plural = count == 1 ? "" : "s";
        System.out.printf("You have %d friend%s\n", count, plural);
    }
    
}
