
package score.student;
import java.util.Scanner;
public class ScoreStudent {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("How many studen?");
        int count = input.nextInt();
        input.nextLine();
        
        int bestScore = -1;
        String studentWithBestScore = "";
        for (int i = 0; i < count; i++) {
            System.out.println("Enter student's name:");
            String name = input.nextLine();
            System.out.println("Enter student's score 0-100:");
            int scorePerc = input.nextInt();
            input.nextLine(); //consume the left over newline character
            if (scorePerc > bestScore) {
                bestScore = scorePerc;
                studentWithBestScore = name;
            }
        }
        System.out.printf("Student %s has the best score %d\n", studentWithBestScore, bestScore);
    }
    
}
