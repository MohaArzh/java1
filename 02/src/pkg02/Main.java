
package day01hello;

import java.util.Scanner;

public class Day01Hello {
    
    public static void main(String[] args) {
       Scanner input = new Scanner(System.in);
        System.out.println("What is your name?");
        String name = input.nextLine();
        System.out.println("How old are you?");
        int age = input.nextInt ();
        System.out.println("Nice to meet you" + name + ", you are " + age + " years old.");
    }
    
}