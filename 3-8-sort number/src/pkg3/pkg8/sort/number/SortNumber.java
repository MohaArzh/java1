
package pkg3.pkg8.sort.number;
import java.util.Scanner;
public class SortNumber {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        System.out.println("Enter first number:");
        int num1 = input.nextInt();
        System.out.println("Enter second number:");
        int num2 = input.nextInt();
        System.out.println("Enter third number:");
        int num3 = input.nextInt();
        
        System.out.println("Your numbers in order are:");
        
        if (num1 < num2 && num1 < num3){
            System.out.println(num1);
        }
        
        else if (num2 < num3 && num2 < num1){
            System.out.println(num2);
        } else {
            System.out.println(num3);
        }
        

        if (num1 > num2 && num1 < num3 || num2 > num1 && num3 < num1){
            System.out.println(num1);
        }
        
        else if (num2 > num1 && num2 < num3 || num3 > num1 && num2 < num1){
            System.out.println(num2);
        } else {
            System.out.println(num3);
        }
        
        if (num1 > num2 && num1 > num3){
            System.out.println(num1);
        }
        
        else if (num2 > num1 && num2 > num3){
            System.out.println(num2);
            
        } else {
            System.out.println(num3);
        }
    }
    
}
