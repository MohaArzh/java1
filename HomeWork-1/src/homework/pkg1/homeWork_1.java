/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework.pkg1;

import java.util.Scanner;

/**
 *
 * @author Muhammad
 */
public class homeWork_1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("How much do you want to save each month?");
        int saving = input.nextInt();
        
        double firstMonth = saving * (1 + 0.00417);
        double secondMonth = (saving + firstMonth) * (1 + 0.00417);
        double thirdMonth = (saving + secondMonth) * (1 + 0.00417);
        double forthMonth = (saving + thirdMonth) * (1 + 0.00417);
        double fifthMonth = (saving + forthMonth) * (1 + 0.00417);
        double sixthMonth = (saving + fifthMonth) * (1 + 0.00417);
        
        System.out.println("Your saving after 3 month will be: " + sixthMonth + ".");
}
}
