
package ifelse.test;
import java.util.Scanner;
public class IfElseTest {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the radius:");
        int radius = input.nextInt ();
if (radius >= 0) {
            double area = radius * radius * 3.14159;
System.out.println("The area for the “ + “circle of radius " + radius +
" is " + area);
} else {
System.out.println("Negative input"); }
  
    }
    
}
