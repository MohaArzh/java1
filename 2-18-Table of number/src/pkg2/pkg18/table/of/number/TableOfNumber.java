
package pkg2.pkg18.table.of.number;
import java.util.Scanner;
public class TableOfNumber {

    public static void main(String[] args) {
       Scanner input = new Scanner(System.in);
       System.out.println("    a     b     pow(a,b)");
        
       for (int i = 0; i < 5; i++) {
            int a = i + 1;
            int b = a + 1;
            double pow = Math.pow(a, b);
            System.out.printf("%5d %5d     %.0f\n", a, b, pow);
        }
  

    }
    
}
