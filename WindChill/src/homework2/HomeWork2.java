
package homework2;

import java.util.Scanner;

public class HomeWork2 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("What is the temperature in Fahrenheit?");
        double temp = input.nextFloat();
        System.out.println("What is the temperature in Fahrenheit?");
        double wind = input.nextFloat();
        
        double windChill = 35.74 + 0.6215 * temp - 35.75 * Math.pow(wind, 0.16) + 0.4275 * temp * Math.pow(wind, 0.16);
        System.out.println("The wind chill index is: " + windChill);
        System.out.printf("the wind chill indes is %.3f\n", windChill);
    }
    
}
