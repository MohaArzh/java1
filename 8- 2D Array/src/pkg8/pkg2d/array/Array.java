package pkg8.pkg2d.array;

import java.util.Arrays;
import java.util.Scanner;

public class Array {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int[][] arrayTwoD = new int[3][4];

        for (int i = 0; i < arrayTwoD.length; i++) {
            for (int j = 0; j < arrayTwoD[i].length; j++) {
                int value = (int) (Math.random() * 101);
                arrayTwoD[i][j] = value;

            }
        }

//        int[][] arrayTwoD = {
//            {1, 2, 3, 4},
//            {5, 6, 7, 8},
//            {9, 10, 11, 12}
//        };
        int sum = 0;
        int counter = 0;
        int largest = 0;

        for (int i = 0; i < arrayTwoD.length; i++) {

            for (int j = 0; j < arrayTwoD[i].length; j++) {
                int value = arrayTwoD[i][j];
                System.out.printf("%s%d", j == 0 ? "" : ", ", value);
                sum += value;
                counter++;
                if (value > largest) {
                    largest = value;
                }
            }
            System.out.println();
        }

        double average = (double) sum / counter;

        System.out.println("Sum of all values is :" + sum);
        System.out.printf("Your Average is: %.2f\n", average);
        System.out.printf("Your largset number is: %d\n", largest);

    }

}
