
package ex.pkg2.pkg17.windchill;

import java.util.Scanner;

public class Ex217Windchill {

    public static void main(String[] args) {
        
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the temperature in Fahrenheit:");
        float temp = input.nextFloat();
        System.out.println("Enter the wind speed in miles per hour:");
        float wind = input.nextFloat();
                        
        double windChill;

        windChill = 35.74 + temp - 35.75 * Math.pow(wind, 0.16) + 0.4275 * temp * Math.pow(wind, 0.16);
        
        System.out.println("The wind chill index is " + windChill + ".");

    }
    
}
