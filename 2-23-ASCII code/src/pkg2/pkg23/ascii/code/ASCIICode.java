
package pkg2.pkg23.ascii.code;
import java.util.Scanner;
public class ASCIICode {

    public static void main(String[] args) {
            Scanner input = new Scanner(System.in);
            
            System.out.println("Enter your number:");
            int num = input.nextInt();
            
            if (num < 97){
                System.out.println("Please enter a number between 97 and 107.");
            } else if (num > 107) {
                System.out.println("Please enter a number between 97 and 107.");
        }
            
            switch (num) {
                case 97:
                    System.out.println("The character is: a");
                    break;
                case 98:
                    System.out.println("The character is: b");
                    break;
                case 99:
                    System.out.println("The character is: c");
                    break;
                case 100:
                    System.out.println("The character is: d");
                    break;
                case 101:
                    System.out.println("The character is: e");
                    break;
                case 102:
                    System.out.println("The character is: f");
                    break;
                case 103:
                    System.out.println("The character is: g");
                    break;
                case 104:
                    System.out.println("The character is: h");
                    break;
                case 105:
                    System.out.println("The character is: i");
                   break;
                case 106:
                    System.out.println("The character is: j");
                    break;
                case 107:
                    System.out.println("The character is: k");
            }

    }
    
}
