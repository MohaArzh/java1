
package homework3;

import java.util.Scanner;


public class HomeWork3 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter your value for meter:");
        double feet = input.nextDouble ();
        double meters = feet * 0.305;
//        System.out.println("Your value for feet is: " + feet + ".");
          System.out.printf("%.3f feet is %.3f meters\n", feet, meters);
    }
    
}
