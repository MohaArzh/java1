package pkg4.pkg1.count.numbers;

import java.util.Scanner;

public class CountNumbers {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int countPositive = 0;
        int countNegative = 0;
        double total = 0;
        while (true) {
            System.out.printf("Enter an integer. Enter 0 to end input: ");
            int whatUserEntered = input.nextInt();

            total += whatUserEntered;

            if (whatUserEntered == 0) {
                break;
            }

            if (whatUserEntered < 0) {
                countNegative++;
            } else if (whatUserEntered > 0) {
                countPositive++;
            }

        }
//        double average = total / (countPositive + countNegative);
        System.out.println("The number of negatives is: " + countNegative);
        System.out.println("The number of positive is: " + countPositive);
        System.out.println("Total: " + total);
        System.out.println("Average: " + total / (countPositive + countNegative));
        System.out.println("Done.");
    }
}
