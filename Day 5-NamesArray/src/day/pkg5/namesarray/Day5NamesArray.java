package day.pkg5.namesarray;

import java.util.Arrays;
import java.util.Scanner;

public class Day5NamesArray {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        
//        System.out.println("How many names do you want to enter?");
//        int numName = input.nextInt();

        String[] namesArray = new String[4];

        for (int i = 0; i < 4; i++) {
            System.out.println("Enter friend's names: ");
            String name = input.nextLine();
            namesArray[i] = name;
        }
        
        for (int i = 0; i < namesArray.length; i++) {
            System.out.println("Friend #" + (i+1) + ":" + namesArray[i]);
        }
//        System.out.println(Arrays.toString(namesArray));

    }

}
