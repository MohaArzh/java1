
package day03compountinterest;

import java.util.Scanner;
public class Day03CompountInterest {

    public static void main(String[] args) {
        final double YEARLY_RATE_PERC = 5.0;
        
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the amount:");
        double monthlyAmount = input.nextDouble();
        System.out.println("Enter how many months:");
        int months = input.nextInt();

        double monthlyMultRate = YEARLY_RATE_PERC / 12 / 100;
        double money = monthlyAmount;
        
        for (int i = 0; i < months; i++){
            System.out.printf("Amount of money at month %d is %.2f\n", i+1, money);
            money *= 1.0 + monthlyMultRate;
            money += monthlyAmount;
        }
        System.out.printf("In the end I have %.2f\n", money);
    }
    
}
