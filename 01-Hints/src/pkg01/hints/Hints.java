package pkg01.hints;

import java.util.Arrays;
import java.util.Scanner;
import java.util.ArrayList;

public class Hints {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        //FOR SOLVING THE PROBLEM WITH INDEX NUMBER WHICH STARTS FROM 0 - START
//                int[] grades = {3, 4, 5, 6, 7, 8, 9, 6, 7, 2};
//                try {
//                    System.out.println(grades[10]);
//                } catch (Exception e) {
//                    System.out.println("e");
//                }
        //FOR SOLVING THE PROBLEM WITH INDEX NUMBER WHICH STARTS FROM 0 - END
        
        
        
        //WHEN WE WANT USER PUTS THE CONTENT OF ARRAY - START
//        System.out.println("Determine how many number do you have?");
//        int size = input.nextInt();
//        int[] gradesNum = new int[size];
//
//        for (int i = 0; i < size; i++) {
//            System.out.println("Enter number: " + (i+1));
//            int x = input.nextInt();
//            gradesNum[i] = x;
//        }
//        System.out.println(Arrays.toString(gradesNum));

        //WHEN WE WANT USER PUTS THE CONTENT OF ARRAY - END
        
        
        
        //2D ARRAY - START
        int [] [] twoD = {
            {1, 2, 3},
            {4, 5, 6, 10, 11},
            {7, 8, 9}
        };
        System.out.println(twoD [1] [1]);
        System.out.println(Arrays.deepToString(twoD));
        
        System.out.println(twoD.length);
        System.out.println(twoD[1].length);
        //2D ARRAY - END
    }
}
