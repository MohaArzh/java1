
package pkgelse.pkgif.test;
import java.util.Scanner;
public class ElseIfTest {

    public static void main(String[] args) {
    Scanner input = new Scanner(System.in);
        System.out.println("enter the score:");
        int score = input.nextInt();
        
        if (score >= 90.0){
           System.out.printf("Score %d is in calss A.\n", score); 
        }
        else if (score >= 80.0){
           System.out.printf("Score %d is in calss B.\n", score); 
        }
        else if (score >= 70.0){
           System.out.printf("Score %d is in calss C.\n", score); 
        }
        else if (score >= 60.0){
           System.out.printf("Score %d is in calss D.\n", score); 
        }
    }
    
}
