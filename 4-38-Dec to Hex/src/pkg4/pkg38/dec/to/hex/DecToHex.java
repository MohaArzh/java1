package pkg4.pkg38.dec.to.hex;

import java.util.Arrays;
import java.util.Scanner;

public class DecToHex {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter decimal integer:");
        int num = input.nextInt();
        int remainder = num % 10;
        System.out.println(remainder);
    }

}
