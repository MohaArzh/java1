package pkg5.pkg2.sum.digit;
import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Arrays;

public class SumDigit {

    public static int sumDigits(int userNum) {
        List<Integer> digits = new ArrayList<Integer>();
        for (int i = 0; i <= digits.size(); i++) {
            int remainder = userNum % 10;
            digits.add(remainder);
            int separate = userNum / 10;
            userNum = separate;
            if (userNum < 1) {
                break;
            }
        }

        //        FIRST SOLUTION - START
        for (int i = 0; i < digits.size(); i++) {
            System.out.print(digits.get(i));
        }
        //        FIRST SOLUTION - END

        
        //        SECOND SOLUTION - START
//        for (int oneDigit : digits) {
//            System.out.println(oneDigit);
//        }
        //        SECOND SOLUTION - END
        
        
        int sumAllDigits = 0;
        for (int i = 0; i < digits.size(); i++) {
            sumAllDigits += digits.get(i);
        }
        
        System.out.println("and the sum of your digits is: " + sumAllDigits);

        return sumAllDigits;

    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Enter your number: ");
        int userNumEnter = input.nextInt();
        System.out.println("Your separated digits are: ");
        
        int total = sumDigits(userNumEnter);
        int total2 = total * 10;
        System.out.println("The return value: " + total2);

    }

}
