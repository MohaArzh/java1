package day.pkg5.numarray;

import java.util.Scanner;
import java.util.Arrays;

public class Day5NumArray {

//METHOD 1 start
    public static int[] enterIntegers(int[] array) {
        Scanner input = new Scanner(System.in);

        for (int i = 0; i < 4; i++) {
            System.out.println("Enter one number: ");
            int number = input.nextInt();
            array[i] = number;

        }
        System.out.println("Your numbers are: " + Arrays.toString(array));
        return array;
    }
//METHOD 1 ENDS
    
//METHOD 2 start  

    public static void findSmallestInteger(int[] array) {

        int smallest = array[0];

        for (int i = 0; i < array.length; i++) {
            int elementArray = array[i];
            if (elementArray < smallest) {
                smallest = elementArray;
            }
        }
        System.out.println("Yor smallest number is: " + smallest);

    }
 //METHOD 2 ENDS 
    
 //METHOD 3 start  

    public static void getSumOfIntegers(int[] array) {

        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            int elementArray = array[i];
            sum += elementArray;
        }
        System.out.println("The sum of your numbers is: " + sum);

    }
 //METHOD 3 ENDS 

 //METHOD 4 start 
    
    public static void findGreatestInteger(int[] array) {
        int greatest = array[0];
        for (int i = 0; i < array.length; i++) {
            int elementArray = array[i];
            if (elementArray > greatest) {
                greatest = elementArray;
            }
        }
        System.out.println("Yor gratest number is: " + greatest);

    }
  //METHOD 4 ENDS
//--------------------------------------------------------------------------//
    
    public static void main(String[] args) {
        int[] valuesArray = new int[4];
        int gratest = valuesArray[0];
        int arrayLengthNum = valuesArray.length;
        
        //IMPLICATION OF METHOD 1       
        enterIntegers(valuesArray);
        //IMPLICATION OF METHOD 2
        findSmallestInteger(valuesArray);
        //IMPLICATION OF METHOD 3    
        getSumOfIntegers(valuesArray);
        //IMPLICATION OF METHOD 4
        findGreatestInteger(valuesArray);

    }

}
