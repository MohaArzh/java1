
package homework;
import java.util.Scanner;


public class HomeWork1 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("How much do you want to save each month?");
        int saving = input.nextInt();
        
        double firstMonth = saving * (1 + 0.00417);
        double secondMonth = firstMonth + saving * (1 + 0.00417);
        double thirdMonth = secondMonth + saving * (1 + 0.00417);
        double forthMonth = thirdMonth + saving * (1 + 0.00417);
        double fifthMonth = forthMonth + saving * (1 + 0.00417);
        double sixthMonth = fifthMonth + saving * (1 + 0.00417);
        
        System.out.println("Your balance after six mounths will be: " + sixthMonth + ".");
    }
    
}
