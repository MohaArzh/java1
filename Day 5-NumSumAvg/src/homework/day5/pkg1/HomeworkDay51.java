package homework.day5.pkg1;

import java.util.Scanner;

public class HomeworkDay51 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int total = 0;
        int countNum = 0;
        
        while (true) {
            System.out.println("Enter a number. Enter 0 to end.");
            int number = input.nextInt();

            if (number == 0) {
                break;
            }
            if (number > 0) {
                countNum++;
                total += number;
            }

        }
        double average = total / countNum;
        System.out.println("Total: " + total);
        System.out.printf("Average: %.0f\n", average);
        System.out.println("Done!");
    }
}
