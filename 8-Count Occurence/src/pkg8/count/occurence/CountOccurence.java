package pkg8.count.occurence;

import java.util.Arrays;
import java.util.Scanner;

public class CountOccurence {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int[] arrayOccurence = new int[6];
        for (int i = 0; i < arrayOccurence.length; i++) {
            System.out.println("Enter your number. Enter 0 to end.");
            int userNum = input.nextInt();
            if (userNum == 0) {
                break;
            }
            arrayOccurence[userNum]++;
        }
        for (int i = 0; i < arrayOccurence.length; i++) {
            int arrayNumber = arrayOccurence[i];
            if (arrayNumber == 0){
                continue;
            }
            System.out.printf("Your duplicate for number %d is %d\n", i, arrayOccurence[i]);

        }
    }

}
