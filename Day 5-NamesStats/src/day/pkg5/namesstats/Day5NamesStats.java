package day.pkg5.namesstats;

import java.awt.datatransfer.DataFlavor;
import java.util.Scanner;

public class Day5NamesStats {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int count = 0;

        while (true) {
            System.out.print("Enter name: ");
            String name = input.nextLine();
            if (name.isEmpty()) {
                break;
            }
            
                for (int i = 0; i < name.length(); i++) {
                    if (name.charAt(i) != ' ') {
                        count++;
                    }
                }
        }


        System.out.println("your num: " + count);

    }

}
